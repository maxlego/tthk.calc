﻿using Calc.ApplicationServices.ServiceResults;
using Calc.ApplicationServices.Tokens;
using Calc.Core.ApplicationServices;
using Calc.Core.Domain;
using Calc.Core.ServiceResults;
using System;
using System.Globalization;
using System.Linq;

namespace Calc.ApplicationServices
{
    public class Singleton
    {
        private static Singleton _instance;

        private Singleton()
        {
        }

        public static Singleton GetInstance()
        {
            return _instance ?? (_instance = new Singleton());
        }
    }

    public class CalculatorService : ICalculatorService
    {
        private readonly OperatorRegistry _opRegistry = new OperatorRegistry();

        public CalculatorService()
        {
            _opRegistry.RegOperator<BinaryCalcNode>(new AddOperator());
            _opRegistry.RegOperator<BinaryCalcNode>(new MultiplyOperator());
            _opRegistry.RegOperator<BinaryCalcNode>(new DivideOperator());
            _opRegistry.RegOperator<BinaryCalcNode>(new SubtractOperator());
        }

        public IServiceResult<decimal> EvalExpression(string expression)
        {
            var res = Parse(expression);
            if (!res.Success)
            {
                return ServiceResult.Error<decimal>(res);
            }

            var value = res.Item.GetValue();
            return ServiceResult.Ok(value);
        }

        private TokenizeResult TokenizeInternal(string text, ref int i, int depth = 0)
        {
            var res = new TokenizeResult();

            if (string.IsNullOrWhiteSpace(text))
            {
                res.AddError("Empty input");
                return res;
            }

            var firstIndex = i;
            var firstChar = text[i];
            var isInParenthesis = false;
            if (depth > 0 && firstChar == '(')
            {
                i++;
                isInParenthesis = true;
            }

            var tokenList = new ArrayToken();

            var currentValue = string.Empty;

            for (; i < text.Length; ++i)
            {
                var c = text[i];

                if (c == '(')
                {
                    var innerExressionResult = TokenizeInternal(text, ref i, depth + 1);
                    res.Errors.AddRange(innerExressionResult.Errors);
                    tokenList.Add(new ArrayToken(innerExressionResult.Item));
                    continue;
                }
                if (c == ')')
                {
                    if (!isInParenthesis)
                    {
                        res.AddError("Opening parenthesis is missing", i);
                        continue;
                    }
                    else
                    {
                        CreateValueToken(i);

                        res.Item = tokenList.ToArray();
                        return res;
                    }
                }

                var opString = c.ToString();
                if (_opRegistry.IsKnowOperator(opString, out IOperator op))
                {
                    CreateValueToken(i);

                    if (op is IBinaryOperator && (tokenList.Count == 0 || tokenList.Last() is OperatorToken))
                    {
                        res.AddError("Left is missing", i);
                    }

                    tokenList.Add(new OperatorToken(op));

                    if (op is IBinaryOperator)
                    {
                        Action<Token> onNextTokenCreated = null;
                        var charIndex = i;
                        onNextTokenCreated = (token) =>
                        {
                            if (token is OperatorToken)
                            {
                                res.AddError("Right is missing", charIndex);
                            }

                            tokenList.TokenAdded -= onNextTokenCreated;
                        };

                        tokenList.TokenAdded += onNextTokenCreated;
                    }
                }
                else
                {
                    currentValue += c;
                }
            }

            CreateValueToken(i);

            if (isInParenthesis)
            {
                res.AddError("Missing end parenthesis", firstIndex);
            }

            void CreateValueToken(int charIndex)
            {
                if (string.IsNullOrWhiteSpace(currentValue))
                {
                    return;
                }

                var value = currentValue.Replace(',', '.');
                if (!decimal.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out _))
                {
                    res.AddError("Invalid number", charIndex - currentValue.TrimStart().Length);
                }

                tokenList.Add(new ValueToken(currentValue.Trim()));

                currentValue = string.Empty;
            }

            res.Item = tokenList.GetTokens();
            return res;
        }

        public TokenizeResult Tokenize(string text)
        {
            var i = 0;
            return TokenizeInternal(text, ref i);
        }

        public IServiceResult<CalcNode> Parse(string v)
        {
            var tokenizeResult = Tokenize(v);
            if (!tokenizeResult.Success)
            {
                return ServiceResult.Error<CalcNode>(tokenizeResult);
            }

            return Parse(tokenizeResult.Item);
        }

        public IServiceResult<CalcNode> Parse(Token[] list)
        {
            OperatorToken highestOperatorToken = GetHighestOperatorToken(list, out var i);
            list = ProcessNode(list, highestOperatorToken, i);

            if (list.Length > 1)
            {
                return Parse(list);
            }

            var calcNode = ((CalcNodeToken)list[0]).CalcNode;
            return ServiceResult.Ok(calcNode);
        }

        public Token[] ProcessNode(Token[] list, OperatorToken highestOperatorToken, int i)
        {
            var prev = list[i - 1];
            var next = list[i + 1];

            if (highestOperatorToken.Operator is IBinaryOperator binary)
            {
                CalcNode left;
                CalcNode right;

                if (prev is ArrayToken arrayToken)
                {
                    var res = Parse(arrayToken.GetTokens());
                    left = res.Item;
                }
                else
                {
                    left = prev is CalcNodeToken calcNodeToken
                        ? calcNodeToken.CalcNode
                        : new ValueCalcNode(((ValueToken)prev).Value);
                }

                if (next is ArrayToken arrayToken2)
                {
                    var res = Parse(arrayToken2.GetTokens());
                    right = res.Item;
                }
                else
                {
                    right = next is CalcNodeToken calcNodeToken
                        ? calcNodeToken.CalcNode
                        : new ValueCalcNode(((ValueToken)next).Value);
                }

                var calcNode = new BinaryCalcNode(binary, left, right);

                return list.Replace(i - 1, i + 1, new CalcNodeToken(calcNode));
            }

            return null;
        }

        public OperatorToken GetHighestOperatorToken(Token[] list, out int highestNodeIndex)
        {
            highestNodeIndex = -1;
            OperatorToken highestNode = null;
            for (var i = 0; i < list.Length; i++)
            {
                if (list[i] is OperatorToken operatorToken)
                {
                    var isHighest = highestNode == null
                        || highestNode.Operator.Priority < operatorToken.Operator.Priority;
                    if (isHighest)
                    {
                        highestNode = operatorToken;
                        highestNodeIndex = i;
                    }
                }
            }

            return highestNode;
        }
    }
}