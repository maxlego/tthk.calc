﻿using Calc.ApplicationServices.Tokens;
using Calc.Core.ApplicationServices;
using Calc.Core.ServiceResults;

namespace Calc.ApplicationServices.ServiceResults
{
    public class TokenizeResult : ServiceResult<Token[]>
    {
        public void AddError(string error, int charIndex = 0)
        {
            Errors.Add(new TokenError
            {
                Error = error,
                CharIndex = charIndex
            });
        }
    }

    public class TokenError : ValidationError
    {
        public int CharIndex { get; set; }
    }
}
