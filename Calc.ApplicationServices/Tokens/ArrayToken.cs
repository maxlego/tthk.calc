﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Calc.ApplicationServices.Tokens
{
    public class ArrayToken : Token, IEnumerable<Token>
    {
        public ArrayToken()
        {
        }

        public ArrayToken(IEnumerable<Token> tokens)
        {
            _tokens.AddRange(tokens);
        }

        private List<Token> _tokens = new List<Token>();
        public int Count => _tokens.Count;

        public void Add(Token token)
        {
            _tokens.Add(token);
            PublishTokenAdded(token);
        }

        private void PublishTokenAdded(Token token)
        {
            TokenAdded?.Invoke(token);
        }

        public event Action<Token> TokenAdded;

        public Token[] GetTokens()
        {
            return _tokens.ToArray();
        }

        public IEnumerator<Token> GetEnumerator()
        {
            return _tokens.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
