﻿using Calc.Core.Domain;

namespace Calc.ApplicationServices.Tokens
{
    public class CalcNodeToken : Token
    {
        public CalcNode CalcNode { get; }

        public CalcNodeToken(CalcNode calcNode)
        {
            CalcNode = calcNode;
        }
    }
}
