﻿using Calc.Core.Domain;

namespace Calc.ApplicationServices.Tokens
{
    public class OperatorToken : Token
    {
        public IOperator Operator { get; }

        public OperatorToken(IOperator @operator)
        {
            Operator = @operator;
        }
    }
}
