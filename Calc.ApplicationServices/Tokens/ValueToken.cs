﻿namespace Calc.ApplicationServices.Tokens
{
    public class ValueToken : Token
    {
        public string Value { get; }

        public ValueToken(string value)
        {
            Value = value;
        }
    }
}
