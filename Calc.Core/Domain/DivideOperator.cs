﻿using Calc.Core.ServiceInterfaces.Operators;
using System;

namespace Calc.Core.Domain
{
    public class DivideOperator : IBinaryOperator
    {
        public string Symbol => "/";

        public int Priority => 2;

        public decimal Exec(decimal left, decimal right)
        {
            return left / right;
        }
    }
}
