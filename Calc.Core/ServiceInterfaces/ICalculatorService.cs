﻿using Calc.Core.ServiceResults;

namespace Calc.Core.ApplicationServices
{
    public interface ICalculatorService
    {
        IServiceResult<decimal> EvalExpression(string expression);
    }
}
