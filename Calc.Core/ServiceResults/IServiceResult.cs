﻿using System.Collections.Generic;

namespace Calc.Core.ServiceResults
{
    public interface IServiceResult
    {
        bool Success { get; }
        List<IValidationError> Errors { get; }
    }

    public interface IServiceResult<TItem> : IServiceResult
    {
        TItem Item { get; }
    }
}
