﻿namespace Calc.Core.ServiceResults
{
    public interface IValidationError
    {
        string Error { get; }
    }

    public class ValidationError : IValidationError
    {
        public string Error { get; set; }
    }
}
