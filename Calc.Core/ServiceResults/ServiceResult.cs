﻿using System.Collections.Generic;

namespace Calc.Core.ServiceResults
{
    public class ServiceResult<TItem> : IServiceResult<TItem>
    {
        public bool Success { get => (Errors?.Count ?? 0) == 0; }
        public List<IValidationError> Errors { get; } = new List<IValidationError>();
        public TItem Item { get; set; }
    }

    public static class ServiceResult
    {
        public static ServiceResult<T> Ok<T>(T item)
        {
            return new ServiceResult<T> { Item = item };
        }

        public static ServiceResult<T> Error<T>(IServiceResult baseResult)
        {
            var res = new ServiceResult<T>();
            res.Errors.AddRange(baseResult.Errors);

            return res;
        }
    }
}
