using Calc.ApplicationServices.ServiceResults;
using Calc.ApplicationServices.Tokens;
using Calc.Core.Domain;
using Xunit;

namespace Calc.Test
{
    public class AddOperator
    {
        [Fact]
        public void Should_BeAbleToAddTwoNumbersTogether()
        {
            var op = new Core.Domain.AddOperator();
            var result = op.Exec(1, 2);
            Assert.Equal(3, result);
        }
    }

    public class CalculatorService
    {
        [Fact]
        public void Should_BeAbleToEvaluateTree()
        {
            var rootNode = new BinaryCalcNode(
                new Core.Domain.AddOperator(),
                new ValueCalcNode(1),
                new BinaryCalcNode(
                    new Core.Domain.MultiplyOperator(),
                    new ValueCalcNode(2),
                    new ValueCalcNode(3)
                )
            );

            var result = rootNode.GetValue();

            Assert.Equal(7, result);
        }

        [Fact]
        public void Should_BeAbleToReprasentTreeAsString()
        {
            var rootNode = new BinaryCalcNode(
                new Core.Domain.AddOperator(),
                new ValueCalcNode(1),
                new BinaryCalcNode(
                    new Core.Domain.MultiplyOperator(),
                    new ValueCalcNode(2),
                    new ValueCalcNode(3)
                )
            );

            var result = rootNode.GetString();

            Assert.Equal("1+2*3", result);
        }

        [Fact]
        public void Should_BeAbleToSplitExpressionToArray()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var splitResult = calcService.Tokenize("1+2*3");
            var array = splitResult.Item;

            Assert.Equal(5, array.Length);

            var expextedArray = new[] {
                "1",
                "+",
                "2",
                "*",
                "3"
            };
            for (var i = 0; i < expextedArray.Length; i++)
            {
                if (array[i] is ValueToken valueToken)
                {
                    Assert.Equal(expextedArray[i], valueToken.Value);
                }
            }
        }

        [Fact]
        public void Should_BeAbleToSplitExpressionToArray_WhenContainingParenthesis()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var splitResult = calcService.Tokenize("(1+2+6)*3");
            var array = splitResult.Item;

            Assert.Equal(3, array.Length);
        }

        [Fact]
        public void Should_BeAbleToSplitExpressionToArray_WhenContainingParenthesis2()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var splitResult = calcService.Tokenize("3*(1+2+6)");
            var array = splitResult.Item;

            Assert.Equal(3, array.Length);
        }

        [Fact]
        public void Should_BeAbleToGetHighestNode()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var splitResult = calcService.Tokenize("1+2*3");
            var array = splitResult.Item;

            var res = calcService.GetHighestOperatorToken(array, out var _);
            Assert.Equal("*", res.Operator.Symbol);
        }

        [Fact]
        public void Should_BeAbleToParseStringToTree()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var node = calcService.Parse("1+2*3").Item;

            Assert.True(node is BinaryCalcNode);
            var binaryCalcNode = (BinaryCalcNode)node;

            Assert.True(binaryCalcNode.Operator is Core.Domain.AddOperator);
            var addOperator = (Core.Domain.AddOperator)binaryCalcNode.Operator;

            Assert.True(binaryCalcNode.Left is ValueCalcNode);
            var addLeft = (ValueCalcNode)binaryCalcNode.Left;

            Assert.Equal(1, addLeft.Value);

            Assert.True(binaryCalcNode.Right is BinaryCalcNode);
            var binaryCalcNode2 = (BinaryCalcNode)binaryCalcNode.Right;

            Assert.True(binaryCalcNode2.Operator is MultiplyOperator);

            Assert.True(binaryCalcNode2.Left is ValueCalcNode);
            Assert.Equal(2, ((ValueCalcNode)binaryCalcNode2.Left).Value);

            Assert.True(binaryCalcNode2.Right is ValueCalcNode);
            Assert.Equal(3, ((ValueCalcNode)binaryCalcNode2.Right).Value);
        }

        [Fact]
        public void Should_BeAbleToEvalExpression()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var res = calcService.EvalExpression("1 + 2 * 3");
            Assert.Equal(7, res.Item);
        }

        [Fact]
        public void Should_BeAbleToUnderstandParenthesis()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var res = calcService.EvalExpression("(1 + 2) * 3");
            Assert.Equal(9, res.Item);
        }

        [Fact]
        public void Should_BeAbleToUnderstandMultipleParenthesis()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var res = calcService.EvalExpression("1 + (1 + 2) * (3 + 1)");
            Assert.Equal(13, res.Item);
        }

        [Fact]
        public void Should_BeAbleToUnderstandNestedParenthesis()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var res = calcService.EvalExpression("2 * ((1 + 2) + (3 + 1))");
            Assert.Equal(14, res.Item);
        }

        [Fact]
        public void Should_BeAbletoDetectMismatchingParenthesis()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var expression = "2 * (2 - (43";

            var splitResult = calcService.Tokenize(expression);

            Assert.False(splitResult.Success);
            Assert.Equal(2, splitResult.Errors.Count);
            Assert.Equal(9, ((TokenError)splitResult.Errors[0]).CharIndex);
            Assert.Equal(4, ((TokenError)splitResult.Errors[1]).CharIndex);
        }

        [Fact]
        public void Should_BeAbletoDetectMismatchingParenthesis2()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var expression = "2 * ) - 2 - (43";

            var splitResult = calcService.Tokenize(expression);

            Assert.False(splitResult.Success);
            Assert.Equal(4, splitResult.Errors.Count);

            Assert.Equal(4, ((TokenError)splitResult.Errors[0]).CharIndex);
            Assert.Equal("Opening parenthesis is missing", ((TokenError)splitResult.Errors[0]).Error);

            Assert.Equal(6, ((TokenError)splitResult.Errors[1]).CharIndex);
            Assert.Equal("Left is missing", ((TokenError)splitResult.Errors[1]).Error);

            Assert.Equal(2, ((TokenError)splitResult.Errors[2]).CharIndex);
            Assert.Equal("Right is missing", ((TokenError)splitResult.Errors[2]).Error);

            Assert.Equal(12, ((TokenError)splitResult.Errors[3]).CharIndex);
            Assert.Equal("Missing end parenthesis", ((TokenError)splitResult.Errors[3]).Error);
        }

        [Fact]
        public void Should_BeAbletoDetectInvalidNumbers()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var expression = "foo +       bar";

            var splitResult = calcService.Tokenize(expression);

            Assert.False(splitResult.Success);
            Assert.Equal(2, splitResult.Errors.Count);
            Assert.Equal(0, ((TokenError)splitResult.Errors[0]).CharIndex);
            Assert.Equal(12, ((TokenError)splitResult.Errors[1]).CharIndex);
        }

        [Fact]
        public void Should_BeAbletoDetectIfBinaryLeftIsMissing()
        {
            var calcService = new ApplicationServices.CalculatorService();
            var expression = " + 2";

            var splitResult = calcService.Tokenize(expression);

            Assert.False(splitResult.Success);
            Assert.Equal(1, splitResult.Errors.Count);
            Assert.Equal(1, ((TokenError)splitResult.Errors[0]).CharIndex);
            Assert.Equal("Left is missing", splitResult.Errors[0].Error);
        }
    }
}
