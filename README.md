# README #



## Kuidas repoot kasutada? ##

* Tehke endale fork
* Oma repos, mis te forkisite, ärge masterisse ise midagi pushige, et vältida tulevikus merge konflikte


## Kodune ylesanne

Teha parooli validaator, mis kontrollib, kas parool on nõrk, keskmine või tugev.  
Valideerimismeetorile antakse tingumused koos parooliga ette.

parameetrid, mida valideerida:

- parooli pikkus
- kas parool sisaldab väikest tähte
- kas parool sisaldab suurt tähte
- kas parool sisaldab numbrit
- kas parool sisaldab sümbolit

Malli saab võtta kontrolltöö demo projektist https://bitbucket.org/maxlego/tthk.randomizer

## Kodune ylesanne 2

Projekt https://bitbucket.org/maxlego/ng-list.  
Implemetreerida kasutajaliideses list itemi kustutamine


## Kontrolltöö

### yl (10p)

kirutada library, mis oskab teha unikaalseid suvalisi täisarve vahemikus 1..100_000. 
max 1000tk 
Kirjutada test

### kysimused

1. mis on yhist ja mis on erinevused abstract klassi ja interfacel (1p)
2. mis on OOP 3 põhilist alustala. Kirjeldus + näited (3p)
3. mida tähendab static (1p)
4. mis on singleton pattern. kirjutada implementatsioon. (2p)
5. mis on generic type. miks seda kasutada. koodinäide (2p)
6. mis on generic type constraint. miks neid kasutatakse? (2p)
7. value type vs reference type. näited (2p)
8. mis on delegate. (1p)
9. mis on Action ja Func vahe (1p)
10. mis on exe ja dll vahe (1p)

Kokku 26p


## 9. nädal (24.10.2018)



### Ülesanne

* Avalidese valideerimine
** operaatoril puudub left või right
** vigane number
** puudub algus või lõpu sulg
** 0-ga jagamine
** liiga suur number

## 8. nädal (17.10.2018)

* value type vs reference type; class vs struct
* ref parameetrid
* out parameetrid
* class vs interface
* abstract class / method / property
* public / private / protected / internal
* exe vs dll
* [aspnetcore middlewares](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/middleware/?view=aspnetcore-2.1)
* [array vs list](https://stackoverflow.com/questions/434761/array-versus-listt-when-to-use-which)

### Ülesanne

Angulari numpad kalkulaatorile

## 7. nädal (10.10.2018)

* [BEM](http://getbem.com/introduction/)

### Ülesanne

* Luua home lehele input väli, kuhu saab sisestada expressioni ja saata see api-le
* kuvada api-st saadud tulemus

## 6. nädal (3.10.2018)

* ++x vs x++
* x() && y() - kui esimene operatsioon on false, siis y() käima ei lasta
* [Singleton pattern](https://en.wikipedia.org/wiki/Singleton_pattern)
* DI (dependency injectoni) lifestyled (singleton, scoped, transient)
* API routing
* HTTP verbs
** PUT
** POST
** GET
** DELETE
* REST

### Ülesanne

* Avalides parsimine peab aru saama mitmetest alam-avaldisest samal tasemel. nt: 1 + (1+2) * (3+1)

## 5. nädal (26.09.2018)

* yield return
* generikud
* sulgude parsimine (lihtsustatud)

## 4. nädal (19.09.2018) ##

* yield return
* anonyymsed funktsioonid

### Ülesanne ###

* Avaldise stringi parsimine puu struktuuriks (pushige oma töö oma reposse ja saatke mulle link)

## 3. nädal (12.09.2018) ##

### Ülesanne ###

* Puust avaldise stringi koostamine

### kasulikud lingid ###

* [Visual studio keyboard shortcuts](https://docs.microsoft.com/en-us/visualstudio/ide/default-keyboard-shortcuts-in-visual-studio?view=vs-2017)
* [aspnetcore](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-2.1)